#!/bin/bash

export COURSE_DIR=$HOME/jupyter-course-nov-19
export PROJECT_DIR=$HOME/sample-project
export NODE_OPTIONS=--max_old_space_size=8192

echo "##################"
echo "# Update Jupyter #"
echo "##################"
sleep 1
pip install -U jupyter jupyterlab --user

echo "############################"
echo "# Download course material #"
echo "############################"
sleep 1
git clone https://gitlab.gwdg.de/jupyter-course/jupyter-course-nov-19.git $COURSE_DIR
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi
git clone https://gitlab.gwdg.de/jupyter-course/sample-project.git $PROJECT_DIR
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

echo "###############################"
echo "# Create virtual environments #"
echo "###############################"
sleep 1
python3 -m venv $COURSE_DIR/env
source $COURSE_DIR/env/bin/activate
pip install -U pip
pip install bokeh==1.4.0 # to avoid "No space left" error
pip install -r $COURSE_DIR/requirements.txt
python -m ipykernel install --name jupyter-course --user
deactivate

python3 -m venv $PROJECT_DIR/env
source $PROJECT_DIR/env/bin/activate
pip install -U pip
pip install -r $PROJECT_DIR/requirements.txt
python -m ipykernel install --name sample-project --user
deactivate

echo "##################"
echo "# Install NodeJS #"
echo "##################"
sleep 1
cd $HOME
touch .bash_profile
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | bash
echo "export NODE_OPTIONS=--max_old_space_size=8192" >> .bash_profile
source $HOME/.bash_profile
nvm install 10

echo "#################################"
echo "# Install jupyterlab extensions #"
echo "#################################"
sleep 1
jupyter labextension install @jupyter-widgets/jupyterlab-manager --no-build
jupyter labextension install @bokeh/jupyter_bokeh --no-build
