"""A minimal setup module.

Adapted from:
https://github.com/pypa/sampleproject
"""

from os import path
from setuptools import setup, find_packages


BASE_PATH = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(BASE_PATH, 'README.md'), encoding='utf-8') as f:
    LONG_DESCRIPTION = f.read()

setup(
    name='FizzBuzz',                                # Required
    version='1.0.0',                                # Required
    description='A sample package',                 # Optional
    long_description=LONG_DESCRIPTION,              # Optional
    long_description_content_type='text/markdown',  # Optional
    author='Nils Luettschwager',                    # Optional
    author_email='nluetts@gwdg.de',                 # Optional
    packages=find_packages(),                       # Required
    python_requires='>3.5, <4',                     # Required
    install_requires=[],                            # Optional
    project_urls={                                  # Optional
        'Source': 'https://gitlab.gwdg.de/...',
    },
)
