import numpy as np

from unittest import TestCase
from unittest.mock import patch
from integrate.integrate import integrate_trapezoidal


class TestIntegrateTrapezoidal(TestCase):

    def setUp(self):
        # initialization to be performed before each test function runs
        self.x = np.arange(0, 10.1, 0.1)

    def test_constant_function(self):
        y = np.full(self.x.size, 5)  # constant function f(x) = 5
        integral = integrate_trapezoidal(self.x, y)
        self.assertEqual(integral, 50, "something went wrong ...")

    def test_constant_function_close_to(self):
        y = np.full(self.x.size, 5)  # constant function f(x) = 5
        integral = integrate_trapezoidal(self.x, y)
        self.assertAlmostEqual(integral, 50)

    def test_quadratic_function_close_to(self):
        y = self.x**2 - 2*self.x + 1  # 2nd order polynomial function
        integral = integrate_trapezoidal(self.x, y)
        # integral between 0 and 10 should be 730/3
        self.assertAlmostEqual(integral, 730/3)

    def test_linear_function_x_reversed(self):
        x = np.arange(10, -0.1, -0.1)  # what happens if we reverse the order?
        y = np.full(x.size, 5)
        integral = integrate_trapezoidal(x, y)
        self.assertAlmostEqual(integral, 50)

    def test_throws_error(self):
        raise Exception("This test blew up.")

    @patch("integrate.integrate.print")
    def test_shows_correct_result(self, print_mock):
        y = np.full(self.x.size, 5)  # constant function f(x) = 5
        integrate_trapezoidal(self.x, y, display_result=True)
        print_mock.assert_called_with("The integral is 5.000e+01.")
