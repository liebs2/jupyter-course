import numpy as np

from bokeh.io import curdoc
from bokeh.models.widgets import Button
from bokeh.plotting import figure, ColumnDataSource


# plot
X = np.arange(100)
Y = np.random.randn(100)
data_source = ColumnDataSource(dict(x=X, y=Y))

fig = figure()
fig.line(x="x", y="y", source=data_source, name="main_plot")


# callback function
def refresh_plot(*args):
    data_source.data['y'] = np.random.randn(100)


# widgets
refresh_button = Button(label="refresh", name="refresh_button")
refresh_button.on_click(refresh_plot)


# add content to document
curdoc().add_root(fig)
curdoc().add_root(refresh_button)
