---
author: Nils Luettschwager
title: Data Evaluation with Python
subtitle: Tools and Good Practice
institute: Georg-August-University Göttingen, Institute of Physical Chemistry

date: 2021-03-24 to 2021-03-29

theme: metropolis
aspectratio: 169
toc: true
...

The course is structured as follows:

* Overture (Day 1):
  * [x] Organizational note
  * [x] Why a programming language instead of Excel/Origin/... ?
  * [x] Using Jupyter (Notebooks, Terminal)
* Part I: software development tools (Day 1 and 2)
  * [x] Environments
  * [x] Version Control
  * [x] Style
  * [ ] Unit tests
* [ ] Homework: build a reproducible project (own environment, version control, tests)
* Part II: scientific python libraries (Day 3)
  * [ ] Pandas
  * [ ] HoloViews
* [ ] Your projects (Day 3)

## Homework: Build a Reproducible Project (1/2)

On <https://jupyter-cloud.gwdg.de> ...

- create a new project (see [environments.ipynb → Project creation workflow](environments.ipynb#Project-creation-workflow))
  - project includes its own environment, requirements.txt, and README.md
  - `pip install` at least Numpy and Matplotlib as dependencies
- use git version control to track changes of your project
  - `git init` right after you created your project
  - exclude your environment from the git repo (see [version-control.ipynb → Ignoring files](version-control.ipynb#Ignoring-files))
- create a notebook, make a plot, and use Markdown to add a description
  - (**optional**) load data and/or perform data transformation/evaluation
- (**optional**) add more content to your repo, e.g. a Python module with your own function that you import in the notebook

## Homework: Build a Reproducible Project (2/2)

- make commits after logical units of work (e.g. after you finish the notebook, add content to the README.md, add a new function to the module, etc.)
- create a remote repository and push your project (<https://gitlab.gwdg.de>)
- clone your project in another folder on <https://jupyter-cloud.gwdg.de>, install it, and verify that it runs as intended (**optionally** do this on your local computer)

- (**optional**) install the course material and sample project on <https://jupyter-cloud.gwdg.de> or your local computer (see the README.md files of these repos)




# Overture

## Organizational Note

* This methods course is part of the PC internship, module M.Che.1321, and worth 0.5 Credits (see [https://hbond.uni-goettingen.de/ma_chem.html](https://hbond.uni-goettingen.de/ma_chem.html))
* Enrol in FlexNow (M.Che.1321.Mk), otherwise your participation cannot be formally acknowledged

## Why a programming language instead of Excel/Origin/... ?

My comment on why I think doing our data evaluation with Python (or a programming language in general) is a good idea.

Discussion of the [sample project](https://gitlab.gwdg.de/jupyter-course/sample-project).

## Using Jupyter

Some skills we need to master when using Jupyter:

* Basic user interface overview
* Basic notebook editing and navigation
* What are kernels?
* Caveat: out-of-order execution
* Using the terminal
* Importing/exporting files

---
