"""
Generate table of contents for Jupyter notebook in Markdown format.

The parameter `skip` controls how many initial headlines are skipped
(e.g. skip the notebook title with skip=1).
"""

import argparse
import json
import textwrap


def get_toc_from_ipynb(filepath, skip=0):
    """Reads ipynb file and returns headlines as markdown link list.

    The parameter `skip` controls how many initial headlines are skipped
    (e.g. skip the notebook title with skip=1).
    """
    with open(filepath, "r", encoding="utf-8") as f:
        raw_json = json.load(f)

    # read headlines from ipynb file
    md_cells = [c for c in raw_json["cells"]
                if c["cell_type"] == "markdown"]
    headlines = [l.strip()
                 for c in md_cells
                 for l in c["source"]
                 if l[0] == "#"]

    # generate markdown output
    md_output = ""
    for h in headlines[skip:]:
        try:
            hash_symbols, text = h.split(" ", 1)
        except ValueError:
            continue
        count = len(hash_symbols)
        ws = " " * (count-1) * 2  # initial whitespace
        slug = text.replace(" ", "-")
        md_output += f"{ws}- [{text}](#{slug})\n"

    return textwrap.dedent(md_output)


def get_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("filepath",
                        type=str,
                        help="path to ipynb file",
                        nargs="+")
    parser.add_argument("-s", "--skip", default=0, type=int)
    return parser.parse_args()


def main():
    args = get_args()
    for fp in args.filepath:
        print("\n", fp)
        print(f'\n<a id="tableofcontents">*Table of contents*</a>\n')
        print(get_toc_from_ipynb(fp, skip=args.skip), "\n")
        print("[&larr; back to index.md](index.md)")
        print("[&uarr; back to TOC](#tableofcontents)")


if __name__ == "__main__":
    main()
