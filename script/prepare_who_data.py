"""
Helper functions to parse data from WHO csv-files into pandas dataframes.
"""
import os

import numpy as np
import pandas as pd

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
DATA_DIR = os.path.join(BASE_DIR, "..", "data")

# file paths
WHO_LIFE_EXPECTANCY_FP = os.path.join(DATA_DIR, "WHO_life_expectancy.csv")
WHO_EXPENDITURE_FP = os.path.join(DATA_DIR, "WHO_health_expenditure.csv")
WHO_POPULATION_FP = os.path.join(DATA_DIR, "WHO_population.csv")
WHO_REGIONS_FP = os.path.join(DATA_DIR, "WHO_regions.csv")


def remove_whitespace(string):
    return string.replace(' ', '').strip()


def parse_float(x):
    try:
        if type(x) == str:
            x = remove_whitespace(x)
        return float(x)
    except ValueError:
        return np.nan


COLUMNS = ("Year", "Both sexes", "Male", "Female",
           "Population (in thousands) total")
CONVERTERS = {k: parse_float for k in COLUMNS}


def convert_columns_to_int64(df, columns, dtype=np.int64):
    for col in df.columns:
        if col in columns:
            df[col] = df[col].astype(dtype)
    return df


def read_life_expectancy():
    """Read data/WHO_life_expectancy.csv into pandas dataframe.

    Only 'life expectancy at birth' columns are read.
    """
    df = pd.read_csv(WHO_LIFE_EXPECTANCY_FP,
                     skiprows=1,
                     converters=CONVERTERS,
                     usecols=range(5))
    return convert_columns_to_int64(df, ["Year"])


def read_expenditure(long_format=False):
    """Read data/WHO_health_expediture.csv into pandas dataframe."""
    df = pd.read_csv(WHO_EXPENDITURE_FP,
                     skiprows=1,
                     index_col="Country")
    df = df.apply(pd.to_numeric, errors="coerce")
    if long_format:
        df = df.reset_index(
            ).melt(id_vars=["Country"],
                   var_name="Year",
                   value_name="Expenditure")
        return convert_columns_to_int64(df, ["Year"], dtype=np.int64)
    else:
        return df


def read_population():
    """Read data/WHO_population.csv into pandas dataframe.

    Only complete dataset for year 2016 is read.
    """
    df = pd.read_csv(WHO_POPULATION_FP,
                     usecols=range(3),
                     converters=CONVERTERS).dropna()
    int_cols = ["Year", "Population (in thousands) total"]
    df.reset_index(inplace=True, drop=True)
    return convert_columns_to_int64(df, int_cols)


def read_regions():
    return pd.read_csv(WHO_REGIONS_FP, sep="\t")


def get_WHO_data():
    """Return data from WHO csv files, compiled into single dataframe."""
    df = read_life_expectancy(
        ).merge(
            read_expenditure(long_format=True),
            on=["Country", "Year"],
            how="outer"
        ).merge(
            read_population(),
            on=["Country", "Year"],
            how="outer"
        ).merge(
            read_regions(),
            on="Country",
            sort=True)
    return convert_columns_to_int64(df, ["Year"])


def get_WHO_data_bokeh_dashboard():
    """Return WHO data formatted for dashboard in plotting_3.ipynb
    
    Returns tuple with complete dataset and year-averaged dataset.
    """
    df_full = get_WHO_data()
    df_full.drop("Population (in thousands) total", axis=1, inplace=True)
    df_pop = read_population().drop("Year", axis=1)
    # add population entry to every row in full dataset
    df_full = df_full.merge(df_pop, on="Country")

    # rename columns
    column_names = {
        "Both sexes": "Life expectancy, both sexes",
        "Male": "Life expectancy, male",
        "Female": "Life expectancy, female",
        "Expenditure": "Health expenditure /US$",
        "Population (in thousands) total": "Population in 2016 (thousands)"
    }

    df_full.rename(columns=column_names, inplace=True)
    df_year_averaged = df_full.groupby("Country").mean().reset_index()
    df_year_averaged = df_year_averaged.merge(read_regions(), on="Country")
    return df_full.dropna(), df_year_averaged.dropna()