# Cheat Sheet

## Environments

Create a new environment in current directory:[^1]

```bash
python3 -m venv my-new-environment
```

Activate the environment:

```bash
source env/bin/activate
```

Install `ipykernel` and other packages you need:

```bash
pip install ipykernel scipy pandas holoviews
```

[^1]: `my-new-environment` = directory name of new environment, arbitrary, often `env` or `venv`

---

Register environment as new Jupyter kernel:[^2]

```bash
python -m ipykernel install --name="my-project" --user
```

Document your dependencies by creating a `requirements.txt` file:

```bash
pip freeze > requirements.txt
```

Install from a `requirements.txt` file:

```bash
pip install -r requirements.txt
```

[^2]: You can choose the kernel name, here `my-project`, freely.

## Version Control with Git

Initialize a new repository:

```bash
git init
```

Display the current status of the repository (e.g. new or changed files):

```bash
git status
```

Stage a file (will be included in the next commit):

```bash
git add <filename>
```

Stage all files:

```bash
git add --all
```

---

Unstage all files (add file path to unstage single file):

```bash
git reset HEAD
```

Show changes to the previous commit (add file path to view diff of specific file):

```bash
git diff
```

Create a new commit:

```bash
git commit -m "commit message"
```

Stage all changes and commit (shortcut):

```bash
git commit -a -m "commit message"
```

---

Revert file back to last commit:

```bash
git checkout <file name>
```

Revert file back to specific commit:

```bash
git checkout <commit hash> -- <file name>
```

List commits:

```bash
git log
```

Add a remote repository to sync your local repository:

```bash
git remote add origin <repo url>
```

---

Push (upload) your commits to a remote repository (first time):

```bash
git push -u origin --all
```

Push your commits to a remote repository:

```bash
git push
```

Clone remote repository to your local machine (or cloud service):

```bash
git clone <repo url>
```

---

Fetch changes in remote repository:

```bash
git fetch
```

Pull (download) changes from remote repository:

```bash
git pull
```