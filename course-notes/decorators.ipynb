{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Decorators\n",
    "\n",
    "2019-11-28"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the [unittest notebook](unittest.ipynb) notebook, we came accross a *decorator*, the `patch()` function from the `unittest.mock` module.\n",
    "\n",
    "What is a decorator?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A decorator is a function that takes another function as its first argument (the decorated function), defines a new function (a wrapper) that does something with the decorated function, and replaces the decorated function with the new wrapper function. By doing so, the decorator modifies the behavior of the original, decorated function. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Admittedly, this sounds awfully abstract, so here is a concrete example: a decorator that modifies a function in such a way that the time it takes to execute this function is displayed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from time import time, sleep"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def timer(func, *args, **kwargs):\n",
    "    \"\"\"Return a function that wraps `func` and times its execution.\"\"\"\n",
    "    def wrapper(*args, **kwargs):  # defining a function inside another\n",
    "        start = time()             # function is perfectly fine\n",
    "        result = func(*args, **kwargs)\n",
    "        print(f\"execution time = {time() - start} s\")\n",
    "        return result\n",
    "    return wrapper  # a function can return another function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is how we could use this function to accomplish the desired effect: first we define a function `add` which we want to time. Then we replace it with the wrapper function that is returned by `timer` when we pass in `add`.\n",
    "The wrapper function will still execute `add`, but it will also print the time it took this function to run:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add(a, b):\n",
    "    return a + b\n",
    "\n",
    "\n",
    "# `add` is replace by the function `wrapper` that is returned when we\n",
    "# pass `add` to `timer`\n",
    "add = timer(add)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "add(3, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This strategy of overwriting a function by a wrapper that adds functionality is so strong and useful that it got its own name&mdash;*function decoration*&mdash;and its own syntax, the *decorator syntax*. Instead of the `add = timer(add)` above, we can put the *decorator* (the function `timer` in this case) with an `@` symbol right above the definition of the function we want to decorate:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@timer\n",
    "def add_2(x):\n",
    "    return x + 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "add_2(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You see how we modified the `add_2` function simply by putting the `@timer` decorator right before the function definition. Function decoration is simple for the programmer to implement once a decorator is available. Here is a powerful example from the standard library:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The [&rarr; `lru_cache` decorator](https://docs.python.org/3/library/functools.html?highlight=lru_cache#functools.lru_cache) is used for caching function results. If a function is pure (always returns the same output for the same input and does not have side effects like writing to disk or displaying something on the screen), caching is a good way of increasing performance when the same calculation is performed several times. The cache decorator attaches a dictionary with results to the function that is decorated and keeps track of input arguments for which the result was already calculated. Let's try it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from functools import lru_cache\n",
    "# once it is full, the \"least recently used\" cache throws out\n",
    "# the result which was used least recently"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following function simulates an expensive function simply by waiting three seconds before returning the result.\n",
    "In reallity, this would be some serious number crunching, of course 😮💪"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Some decorators take arguments. For lru_cache you have to pass\n",
    "# the maximum number of function calls that can be cached to the decorator.\n",
    "# A power of 2 works best, see the lru_cache documentation.\n",
    "@lru_cache(32)\n",
    "def expensive_function(a, b):\n",
    "    sleep(3)  # do an expensive calculation\n",
    "    return a + b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we call our expensive function, sure enough, we have to wait for three seconds:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "expensive_function(1, 2)  # this will be slow"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "expensive_function(3, 4)  # this will be slow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we call it again with the same arguments, however, the caching mechanism steps in and returns the result from the dictionary that was attached to the function, instead of calling the decorated function. This dictionary lookup is much faster than running the actual function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "expensive_function(1, 2)  # this will be fast"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a last usage example for decorators, showing that it is also possible to stack them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@timer\n",
    "@lru_cache(4096)\n",
    "def expensive_function_2(a, b):\n",
    "    sleep(2.5)\n",
    "    return a + b"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "expensive_function_2(1, 2)  # this will be slow, this time with proof"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "expensive_function_2(1, 2)  # this will be fast, this time with proof"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jupyter-course",
   "language": "python",
   "name": "jupyter-course"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
